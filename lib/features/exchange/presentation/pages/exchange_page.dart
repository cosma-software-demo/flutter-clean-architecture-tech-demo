import 'dart:ffi';

import 'package:flutter/material.dart';

class ExchangePage extends StatefulWidget {
  final String cryptocurrencyName;
  final double cryptoCurrencyUsdValue;

  const ExchangePage(this.cryptocurrencyName, this.cryptoCurrencyUsdValue)
      : super(key: null);

  @override
  _ExchangePageState createState() => _ExchangePageState();
}

class _ExchangePageState extends State<ExchangePage> {
  final TextEditingController cryptoController = TextEditingController();
  final TextEditingController usdController = TextEditingController();

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(widget.cryptocurrencyName),
            TextField(
              controller: cryptoController,
              keyboardType: TextInputType.number,
              onChanged: (text) {
                if (text.isEmpty) {
                  usdController.text = '';
                } else {
                  double val = double.parse(text);

                  usdController.text =
                      (val * widget.cryptoCurrencyUsdValue).toString();
                }
              },
            ),
            const Text('USD'),
            TextField(
              controller: usdController,
              keyboardType: TextInputType.number,
              onChanged: (text) {
                if (text.isEmpty) {
                  cryptoController.text = '';
                } else {
                  double val = double.parse(text);

                  cryptoController.text =
                      (val / widget.cryptoCurrencyUsdValue).toString();
                }
              },
            ),
          ],
        ),
      );
}
