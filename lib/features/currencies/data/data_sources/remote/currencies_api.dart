import 'package:cryptoplease_tech_demo/features/currencies/data/model/currency_model.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'currencies_api.g.dart';

@RestApi(baseUrl: '')
abstract class CurrenciesApi {
  factory CurrenciesApi(Dio dio, {required String baseUrl}) = _CurrenciesApi;

  @GET('/coins/{currency}')
  Future<HttpResponse<CurrencyModel>> fetchCurrency(@Path("currency") String currency);
}
