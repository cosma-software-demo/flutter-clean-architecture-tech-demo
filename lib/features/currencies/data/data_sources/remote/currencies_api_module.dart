import 'package:cryptoplease_tech_demo/core/dependency_injection/base_url.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

import 'currencies_api.dart';

@module
abstract class CurrenciesApiModule {
  CurrenciesApi get(Dio dio, BaseUrl baseUrl) =>
      CurrenciesApi(dio, baseUrl: baseUrl.url);
}