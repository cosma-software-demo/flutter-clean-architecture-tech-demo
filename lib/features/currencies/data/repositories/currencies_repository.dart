import 'package:cryptoplease_tech_demo/core/app_exception.dart';
import 'package:cryptoplease_tech_demo/features/currencies/data/data_sources/remote/currencies_api.dart';
import 'package:cryptoplease_tech_demo/features/currencies/data/model/currency_model.dart';
import 'package:injectable/injectable.dart';

@Injectable()
class CurrenciesRepository {
  final CurrenciesApi api;
  final List supportedCurrenciesNames = [
    'solana',
    'bitcoin',
    'cardano',
    'chainlink',
    'decentraland',
  ];

  CurrenciesRepository(this.api);

  Future<List<CurrencyModel>> getSupportedCurrencies() async {
    List<CurrencyModel> currencies = [];

    for (var element in supportedCurrenciesNames) {
      try {
        currencies.add((await api.fetchCurrency(element)).data);
      } catch (e) {
        throw AppException(e, null);
      }
    }

    return currencies;
  }
}
