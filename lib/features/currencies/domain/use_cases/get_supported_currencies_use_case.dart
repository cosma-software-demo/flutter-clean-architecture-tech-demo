import 'package:cryptoplease_tech_demo/features/currencies/data/repositories/currencies_repository.dart';
import 'package:cryptoplease_tech_demo/features/currencies/domain/entity/currency.dart';
import 'package:cryptoplease_tech_demo/features/currencies/domain/mapper/currency_mapper.dart';
import 'package:injectable/injectable.dart';

@Injectable()
class GetSupportedCurrenciesUseCase {
  final CurrenciesRepository repository;
  final CurrencyMapper mapper;

  GetSupportedCurrenciesUseCase(this.repository, this.mapper);

  Future<List<Currency>> get() async {
    final supportedCurrenciesModels = await repository.getSupportedCurrencies();
    return mapper.toCurrencyList(supportedCurrenciesModels);
  }
}
