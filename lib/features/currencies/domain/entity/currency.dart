import 'package:freezed_annotation/freezed_annotation.dart';

part 'currency.freezed.dart';

@freezed
class Currency  with _$Currency {
  factory Currency.fact(String name, String usdValue) = _Currency;
}