import 'package:cryptoplease_tech_demo/features/currencies/domain/entity/currency.dart';
import 'package:cryptoplease_tech_demo/features/currencies/presentation/ui_models/currency_page_ui_model.dart';
import 'package:equatable/equatable.dart';

abstract class CurrencyPageState extends Equatable {}

class InitialCurrencyPageState extends CurrencyPageState {
  final CurrencyPageUiModel dummyUiModel;

  InitialCurrencyPageState(this.dummyUiModel);

  @override
  List<Object?> get props => [dummyUiModel];
}

class LoadingCurrencyPageState extends CurrencyPageState {
  @override
  List<Object?> get props => [];
}

class LoadedCurrencyPageState extends CurrencyPageState {
  final CurrencyPageUiModel uiModel;

  LoadedCurrencyPageState(this.uiModel);

  @override
  List<Object?> get props => [uiModel];
}

class ErrorCurrencyPageState extends CurrencyPageState {
  @override
  List<Object?> get props => [];
}

class NavigateToExchangePageCurrencyPageState extends CurrencyPageState {
  final Currency currency;

  NavigateToExchangePageCurrencyPageState(this.currency);

  @override
  List<Object?> get props => [currency];
}
