import 'package:cryptoplease_tech_demo/features/currencies/domain/entity/currency.dart';
import 'package:cryptoplease_tech_demo/features/currencies/domain/use_cases/get_supported_currencies_use_case.dart';
import 'package:cryptoplease_tech_demo/features/currencies/presentation/ui_models/currency_page_ui_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import 'currencies_page_states.dart';

@Injectable()
class CurrenciesPageCubit extends Cubit<CurrencyPageState> {
  GetSupportedCurrenciesUseCase getSupportedCurrenciesUseCase;
  late CurrencyPageUiModel lastModel;

  // To Avoid assisted injection we can create a initialState Module
  CurrenciesPageCubit(
    this.getSupportedCurrenciesUseCase,
    InitialCurrencyPageState initialState,
  ) : super(initialState) {
    lastModel = initialState.dummyUiModel;
  }

  Future<void> loadData() async {
    emit(LoadingCurrencyPageState());

    try {
      lastModel =
          CurrencyPageUiModel.fact(await getSupportedCurrenciesUseCase.get());
      emit(
        LoadedCurrencyPageState(lastModel),
      );
    } catch (e) {
      emit(ErrorCurrencyPageState());
    }
  }

  void onLoadExchangePage(Currency currency) {
    emit(NavigateToExchangePageCurrencyPageState(currency));
  }
}
