import 'package:cryptoplease_tech_demo/features/currencies/presentation/cubits/currencies_page_states.dart';
import 'package:cryptoplease_tech_demo/features/currencies/presentation/ui_models/currency_page_ui_model.dart';
import 'package:injectable/injectable.dart';

@module
abstract class CurrenciesPageInitialStateModule {
  InitialCurrencyPageState get dio =>
      InitialCurrencyPageState(CurrencyPageUiModel.fact([]));
}
