import 'package:cryptoplease_tech_demo/features/currencies/domain/entity/currency.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'currency_page_ui_model.freezed.dart';

@freezed
class CurrencyPageUiModel with _$CurrencyPageUiModel{
  factory CurrencyPageUiModel.fact(List<Currency> currencies) = _CurrencyPageUiModel;
}
