import 'dart:io';

import 'package:flutter/material.dart';
import 'package:move_to_background/move_to_background.dart';

class MoveToBackGroundWidget extends StatelessWidget {
  final Widget child;

  const MoveToBackGroundWidget({required this.child, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: () async {
          if (Platform.isAndroid) {
            if (Navigator.of(context).canPop()) {
              return Future.value(true);
            } else {
              await MoveToBackground.moveTaskToBack();
              return Future.value(false);
            }
          } else {
            return Future.value(true);
          }
        },
        child: child,
      );
}
