// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:dio/dio.dart' as _i5;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../features/currencies/data/data_sources/remote/currencies_api.dart'
    as _i8;
import '../../features/currencies/data/data_sources/remote/currencies_api_module.dart'
    as _i15;
import '../../features/currencies/data/repositories/currencies_repository.dart'
    as _i9;
import '../../features/currencies/domain/mapper/currency_mapper.dart' as _i4;
import '../../features/currencies/domain/use_cases/get_supported_currencies_use_case.dart'
    as _i10;
import '../../features/currencies/presentation/cubits/currencies_page_cubit.dart'
    as _i11;
import '../../features/currencies/presentation/cubits/currencies_page_initial_state_module.dart'
    as _i14;
import '../../features/currencies/presentation/cubits/currencies_page_states.dart'
    as _i6;
import '../../features/currencies/presentation/pages/currencies_page.dart'
    as _i12;
import '../../main.dart' as _i7;
import 'base_url.dart' as _i3;
import 'modules/dio_module.dart' as _i13;

const String _prod = 'prod';
// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final dioModule = _$DioModule();
  final currenciesPageInitialStateModule = _$CurrenciesPageInitialStateModule();
  final currenciesApiModule = _$CurrenciesApiModule();
  gh.factory<_i3.BaseUrl>(() => _i3.ProdBaseUrl(), registerFor: {_prod});
  gh.factory<_i4.CurrencyMapper>(() => _i4.CurrencyMapper());
  gh.factory<_i5.Dio>(() => dioModule.dio);
  gh.factory<_i6.InitialCurrencyPageState>(
      () => currenciesPageInitialStateModule.dio);
  gh.factory<_i7.MyApp>(() => _i7.MyApp());
  gh.factory<_i8.CurrenciesApi>(
      () => currenciesApiModule.get(get<_i5.Dio>(), get<_i3.BaseUrl>()));
  gh.factory<_i9.CurrenciesRepository>(
      () => _i9.CurrenciesRepository(get<_i8.CurrenciesApi>()));
  gh.factory<_i10.GetSupportedCurrenciesUseCase>(() =>
      _i10.GetSupportedCurrenciesUseCase(
          get<_i9.CurrenciesRepository>(), get<_i4.CurrencyMapper>()));
  gh.factory<_i11.CurrenciesPageCubit>(() => _i11.CurrenciesPageCubit(
      get<_i10.GetSupportedCurrenciesUseCase>(),
      get<_i6.InitialCurrencyPageState>()));
  gh.factory<_i12.CurrenciesPage>(
      () => _i12.CurrenciesPage(get<_i11.CurrenciesPageCubit>()));
  return get;
}

class _$DioModule extends _i13.DioModule {}

class _$CurrenciesPageInitialStateModule
    extends _i14.CurrenciesPageInitialStateModule {}

class _$CurrenciesApiModule extends _i15.CurrenciesApiModule {}
